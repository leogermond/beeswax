# Beeswax SDL2 fast prototyping engine
# Copyright (C) 2020 Léo Germond
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import logging
import ctypes
import os

log = logging.getLogger("SDL")


class SDLError(Exception):
    """Error raised by the SDL2 library.

    It is possible for multiple errors to occur before calling
    `SDL_GetError() <https://wiki.libsdl.org/SDL_GetError>`_, then
    only the last error is returned.

    The returned string is statically allocated and must not be freed by the
    application.
    """
    @classmethod
    def get_from_SDL(cls, prefix, f):
        """Get error from the
        `SDL_GetError() <https://wiki.libsdl.org/SDL_GetError>`_ function.

        :param prefix: The string prefix for the calling module that catched
        the error.
        :param f: The function in error.
        """
        m = sdl2.SDL_GetError().decode()
        sdl2.SDL_ClearError()
        return cls(f"{prefix}.{f.__name__}(): {m}")


def assert_first_arg_not_null(f):
    """ Assert first arg is not NULL when calling """
    def f_(p, *a, **kw):
        assert p
        return f(p, *a, **kw)
    return f_


def nth_arg_is_cstr(i):
    """Decorator-constructor: The n'th argument is a NULL-terminated C string.

    When calling the function, the ``str()`` representation of the argument
    will be used, and encoded for compatibility with ctypes.
    """
    def f__(f):
        """ Convert str arg to bytes when calling """
        def f_(*a, **kw):
            a2 = (*a[:i], str(a[i]).encode(), *a[i+1:])
            return f(*a2, **kw)
        return f_
    return f__


def is_c_pointer(cls):
    """Checks if the given class is a ctype's pointer class
    """
    return cls is not None and \
        any(issubclass(cls, ptr_cls)
            for ptr_cls in [
                            ctypes._Pointer,
                            ctypes._CFuncPtr,
                            ctypes.c_char_p,
                            ctypes.c_void_p,
                            ctypes.c_wchar_p,
                            ctypes.py_object,
                           ])


class SDLLibGenericWrapper:
    """Generic wrapper around a SDL2 library.

    The wrapper takes care of encoding / decoding of strings, casting to proper
    types, error handling, arguments checking.
    """
    def __init__(self, prefix, search_space):
        """Constructor
        :param prefix: Name of the function prefix in C, eg. for ``SDL_...()``
        this is ``"SDL"``.
        :param search_space: Module or object to search members in.
        """
        self.prefix = prefix
        self.search_space = search_space

    def __getattr__(self, name):
        name_adapt = name
        if not any(c.isupper() for c in name):
            name_adapt = ''.join(word.title() for word in name.split('_'))

        if hasattr(self.search_space, f"{self.prefix}{name_adapt}"):
            # handles SDLK_xxx
            return getattr(self.search_space, f"{self.prefix}{name_adapt}")

        a = getattr(self.search_space, f"{self.prefix}_{name_adapt}")

        if isinstance(a, ctypes._CFuncPtr):
            a = self.magic_decorate(a)
        return a

    def error_returns_value(self, value):
        """Decorator-constructor for function returning a specific value for errors.

        :param value: The error value.
        :return: A decorator handling functions to raise :class:`SDLError` in \
                case the result is the given value.
        """
        def error_returns_value_(f):
            def f_(*a, **kw):
                r = f(*a, **kw)
                if r == value:
                    raise SDLError.get_from_SDL(self.prefix, f)
                return r
            return f_
        return error_returns_value_

    def error_returns_NULL(self, f):
        """Decorator for function returning a NULL pointer for errors.

        In case the function returns ``NULL``, :class:`SDLError` is raised.
        """
        def f_(*a, **kw):
            r = f(*a, **kw)
            if not r:
                raise SDLError.get_from_SDL(self.prefix, f)
            return r
        return f_

    def magic_decorate(self, cf):
        """Decorate a ctypes function automatically by getting hints from its
        name, return type, argument types.
        """
        # Warning: check properties of cf but wrap f
        f = cf
        log.debug(f"wrapping {cf.__name__} := "
                  f"{getattr(cf.restype, '__name__', 'void')} <= "
                  f"({', '.join(a.__name__ for a in cf.argtypes or ())})")

        if is_c_pointer(cf.restype):
            log.debug("  - error returns NULL")
            f = self.error_returns_NULL(f)

        if cf.argtypes:
            if is_c_pointer(cf.argtypes[0]):
                log.debug("  - arg[0] is ptr")
                f = assert_first_arg_not_null(f)

            for i, t in enumerate(cf.argtypes):
                if issubclass(t, ctypes.c_char_p):
                    log.debug(f"  - arg[{i}] is cstr")
                    f = nth_arg_is_cstr(i)(f)
        return f


class SDLLibMainWrapper(SDLLibGenericWrapper):
    """Wrapper specifically for SDL2 library, not any of its sublibrary.

    This wrapper keeps the same goal as :class:`SDLLibGenericWrapper` but it
    has more insight into the underlying functions and adds niceties.
    """
    def poll_event(self, event):
        return self.search_space.SDL_PollEvent(ctypes.byref(event))


# For some builds we do not need the objects (eg sphinx)
has_objects = not bool(os.environ.get("READTHEDOCS", False))

if has_objects:
    import sdl2
    sdl = SDLLibMainWrapper("SDL", sdl2)
    KMOD = SDLLibMainWrapper("KMOD", sdl2)

    try:
        import sdl2.sdlimage
        sdlimg = SDLLibGenericWrapper("IMG", sdl2.sdlimage)
    except ImportError:
        sdlimg = None
else:
    sdl, KMOD, sdlimg = (None,) * 3
