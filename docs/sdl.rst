.. _sdl:

Low-level SDL2 Wrapper
======================

Philosophy
----------

The main idea behind the low-level module is to provide a wrapper that do not adds any
function or logic but will add:

* Casts to C types from python objects
* pathlib support
* Exception-based error handling
* flags handling ?
* Named constants for various magic numbers
* Namespaced functions: ``SDL_Init()`` -> ``sdl.init()``
* Event watch, hint callback…

While garanteeing good compatibility with several platforms and version through
the use of semi-automatic API-discovery and automated unit-testing.


Programming Interface
---------------------

.. module:: beeswax.sdl

This part of the documentation covers the low-level interface of Beeswax. This
module is only in charge of providing the most simple wrapping around the
SDL2 library.

.. autoclass:: SDLLibGenericWrapper
   :inherited-members:

.. autoexception:: SDLError
