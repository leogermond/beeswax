#!/usr/bin/env python
# Learn more: https://github.com/kennethreitz/setup.py
#        and: https://github.com/psf/requests/blob/master/setup.py
import os
import sys
from pathlib import Path

from codecs import open

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand

here = Path(__file__).resolve().parent


class Tox(TestCommand):
    def run_tests(self):
        import subprocess
        sys.exit(subprocess.check_call(['tox']))


# 'setup.py publish' shortcut.
if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist bdist_wheel')
    os.system('twine upload dist/*')
    sys.exit()

packages = find_packages(exclude=["tests", "*.tests", "*.tests.*", "tests.*"])

with open(here / 'requirements.txt') as f:
    requires = f.read().splitlines()

about = {}
with open(here / 'beeswax' / '__version__.py', 'r', 'utf-8') as f:
    exec(f.read(), about)

with open(here / 'README.md', 'r', 'utf-8') as f:
    readme = f.read()
with open(here / 'HISTORY.md', 'r', 'utf-8') as f:
    history = f.read()

setup(
    name=about['__title__'],
    version=about['__version__'],
    description=about['__description__'],
    long_description=readme,
    long_description_content_type='text/markdown',
    author=about['__author__'],
    author_email=about['__author_email__'],
    url=about['__url__'],
    packages=packages,
    package_data={'': ['LICENSE', 'NOTICE'], 'beeswax': ['*.pem']},
    package_dir={'beeswax': 'beeswax'},
    include_package_data=True,
    python_requires=">=3.7",
    install_requires=requires,
    license=about['__license__'],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: Implementation :: CPython',
        'Programming Language :: Python :: Implementation :: PyPy'
    ],
    cmdclass={'test': Tox},
    project_urls={
        'Documentation': 'https://beeswax.readthedocs.io',
        'Source': 'https://gitlab.com/leogermond/beeswax',
    },
)
