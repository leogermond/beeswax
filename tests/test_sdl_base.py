# Beeswax SDL2 fast prototyping engine
# Copyright (C) 2020 Léo Germond
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import pytest
import ctypes

from beeswax import sdl, SDLError


def get_current_test():
    full_name = os.environ.get('PYTEST_CURRENT_TEST').split(' ')[0]
    test_file = full_name.split("::")[0].split('/')[-1].split('.py')[0]
    test_name = full_name.split("::")[1]

    return full_name, test_file, test_name


@pytest.fixture
def sdl_video_init():
    sdl.init(sdl.INIT_VIDEO)
    yield
    sdl.quit()


@pytest.fixture
def sdl_window(sdl_video_init):
    w = sdl.create_window(get_current_test()[2],
                          sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
                          640, 480, 0)
    yield w
    sdl.destroy_window(w)


@pytest.fixture
def sdl_basic_renderer(sdl_window):
    r = sdl.create_renderer(sdl_window, -1, 0)
    yield r
    sdl.destroy_renderer(r)


@pytest.mark.MT_unsafe
class TestCaseSDLWrapper:
    def test_sdl_init_video(self, sdl_video_init):
        pass

    def test_sdl_create_window(self, sdl_window):
        assert sdl_window

    def test_sdl_create_renderer(self, sdl_basic_renderer):
        assert sdl_basic_renderer

    def test_sdl_fail_create_renderer(self, sdl_window):
        # A renderer at the 1000th position is highly improbable
        with pytest.raises(SDLError):
            sdl.create_renderer(sdl_window, 1000, 0)

    def test_sdl_clear_pixels_black(self, sdl_basic_renderer):
        px = ctypes.c_uint32()
        sdl.render_read_pixels(sdl_basic_renderer, sdl.Rect(0, 0, 1, 1),
                               sdl.PIXELFORMAT_RGBA8888, ctypes.pointer(px), 4)
        assert px.value & 0xffffff00 == 0

    def test_sdl_draw_green_pixel(self, sdl_basic_renderer):
        sdl.set_render_draw_color(sdl_basic_renderer, 0, 255, 0,
                                  sdl.ALPHA_OPAQUE)
        sdl.render_draw_point(sdl_basic_renderer, 0, 0)

        px = (ctypes.c_uint32 * 2)()
        sdl.render_read_pixels(sdl_basic_renderer, sdl.Rect(0, 0, 1, 2),
                               sdl.PIXELFORMAT_RGBA8888, ctypes.pointer(px), 4)
        assert px[0] == 0x00ff00ff
        assert px[1] & 0xffffff00 == 0
