.. Beeswax documentation master file, created by
   sphinx-quickstart on Sat Jul 18 14:41:30 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Beeswax's documentation
=======================

.. image:: beeswax.jpg

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Low-level SDL2 Wrapper
======================

At the lowest level is a wrapper around the SDL2 library.

.. toctree::
   :maxdepth: 2

   sdl

Developpers' informations
=========================

.. toctree::
   :maxdepth: 2

   dev

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
