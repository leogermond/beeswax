Release History
===============

dev
---

-   \[Short description of non-trivial change.\]
- Sphinx and readthedoc support
- PyTest support
- Low-level SDL2 wrapping
- Test and doc for SDL wrapping
